import axios from 'axios'

export default () => {
    return axios.create({
        baseURL: 'http://dummy.restapiexample.com/api/v1/employees',
        withCredentials: false,
        headers: {
            // Accept: "application/json",
            //"Content-Type": "application/json"
            "Content-Type": undefined
        }
    });
}