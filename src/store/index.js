import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import api from '@/service/api'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    users: [],
    count: true,
    editcount: false,
  },
  getters:{
    doneUsers: state => {
      return state.users
    },

    USERS(state){
      return state.users
    }
  },
  mutations: {
    increment(state){
      state.count = true
    },
    decrement(state){
      state.count = false;
    },
    editdecrement(state){
      state.editcount = false
    },
    editmodal(state){
      state.editcount = true
    },

    SET_USERS_TO_STATE: (state, users) => {
      state.users = users;
    },
    ADD_USER(state, user){
      let users = state.users.concat(user);
      state.users = users;
    },
    DELETE_USER(state, userId){
      let users = state.users.filter(v => v.id != userId)
      state.users = users
    }
  },
  actions: {
    GET_USERS_FROM_API({commit}){
      return axios('http://dummy.restapiexample.com/api/v1/employees', {
        method: "GET"
      })
      .then((users)=> {
        commit('SET_USERS_TO_STATE', users.data);
        return users
      })
      .catch((error)=>{
        console.log(error);
        return error;
      })
    },
    async createUser({commit}, user){
      let response = await api().post('http://dummy.restapiexample.com/api/v1/create', {"name":"test","salary":"123","age":"23"})
      let savedUser = response.data;
      commit('ADD_USER', savedUser)
      return savedUser;
    },
    async deleteUser({commit}, user) {
      let response = await api().delete(`/employee/${user.id}`)
      if(response.status == 404 ){
        commit('DELETE_USER', user.id);
      }
      
    }
  },
  modules: {
  }
})
